var layOutDay = function() {
    // var events = [ {start: 440, end: 490}, {start: 400, end: 500}, {start: 30, end: 150},
    //     {start: 390, end: 450}, {start: 460, end: 520}, {start: 510, end: 570},
    //     {start: 290, end: 450}, {start: 360, end: 420}, {start: 610, end: 700},
    //     {start: 490, end: 600}, {start: 560, end: 620}, {start: 610, end: 670},
    //     {start: 40, end: 180}, {start: 460, end: 540}, {start: 460, end: 540}, {start: 440, end: 500},
    //     {start: 440, end: 460},
    //     {start: 200, end: 250}];

    var events = [
        { start: 440, end: 490 }, { start: 400, end: 500 }, { start: 30, end: 150 },
        { start: 390, end: 450 }, { start: 460, end: 520 }, { start: 510, end: 570 }, { start: 510, end: 570 }, { start: 510, end: 570 },
        { start: 290, end: 450 }, { start: 360, end: 420 }, { start: 610, end: 700 },
        { start: 490, end: 600 }, { start: 560, end: 620 }, { start: 610, end: 670 },
        { start: 40, end: 180 }, { start: 420, end: 540 }, { start: 200, end: 250 }
    ];

    // var events = [ {start: 440, end: 490}, {start: 400, end: 500}, {start: 30, end: 150},
    //     {start: 390, end: 450}, {start: 460, end: 520}, {start: 510, end: 570},
    //     {start: 290, end: 450}, {start: 360, end: 420}, {start: 610, end: 700},
    //     {start: 40, end: 180}, {start: 460, end: 540}, {start: 460, end: 540}, {start: 200, end: 250}];

    // var events = [
    //     { start: 0, end: 120 },
    //     { start: 60, end: 120 },
    //     { start: 60, end: 180 },
    //     { start: 150, end: 240 },
    //     { start: 200, end: 240 },
    //     { start: 300, end: 420 },
    //     { start: 300, end: 720 },
    //     { start: 360, end: 420 }
    // ];

    // var events = [
    //     { start: 30, end: 90 },
    //     { start: 30, end: 120 },
    //     { start: 30, end: 90 },
    //     { start: 30, end: 90 },
    //     { start: 90, end: 120 },
    //     { start: 90, end: 120 },
    //     { start: 90, end: 120 },
    //     { start: 90, end: 120 },
    //     { start: 500, end: 600 },
    //     { start: 550, end: 720 },
    //     { start: 650, end: 720 },
    //     { start: 300, end: 450 }
    // ];

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////// Utility function

// To check if:
//  - an event is conflicting with another event
//  or
//  - an event is conflicting with events that are stored in an array of events

    function isEventsConflicting(event, eventOrArrayOfEvents) {
        if(!(eventOrArrayOfEvents instanceof Array)) {
            // We are comparing an event to another event
            var event1 = event,
                event2 = eventOrArrayOfEvents;

            return event1.end >= event2.start && event2.end >= event1.start || event2.end >= event1.start && event1.end >= event2.start

        } else {
            // We are comparing an event to an array of events
            var arrayOfEvents = eventOrArrayOfEvents;
            var arrayOfConflictingEvents = [];
            for(var i = 0; i < arrayOfEvents.length; i++) {
                if(isEventsConflicting(event, arrayOfEvents[i])) {
                    arrayOfConflictingEvents.push(arrayOfEvents[i]);
                }
            }
            return arrayOfConflictingEvents;
        }
    }

    function setSmallestWidth(arrayOfEvents) {
        arrayOfEvents.sort(function(event1, event2) { return event1.width > event2.width});
        arrayOfEvents.forEach(function(event){
            arrayOfEvents[0].width? event.width = arrayOfEvents[0].width : '';
        });
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////// Find out conflicting events, non-conflicting events and sort them out

    var conflictingCouples = [];
    var nonConflictingEvents = [];
    var eventsInColumns = [];

// First extract the couples of conflicting events (2 by 2), and the list of all non-conflicting events

    function extractConflictingEvents(events) {
        for(var i = 0; i < events.length - 1; i++) {
            for(var j = i + 1; j < events.length; j++) {
                if (isEventsConflicting(events[i], events[j])) {
                    events[i].isInConflict = true;
                    events[j].isInConflict = true;
                    conflictingCouples.push([events[i], events[j]]);
                }
            }
        }
        console.log('---conflictingCouples', conflictingCouples);
    }

    function extractNonConflictingEvents(events) {
        for(var i = 0; i < events.length; i++) {
            if(!events[i].isInConflict) {
                events[i].width = 1;
                nonConflictingEvents.push(events[i]);
            }
        }
        console.log('---nonConflictingEvents', nonConflictingEvents);
    }

// We will now sort the events by columns
// We put the list of non-conflicting events as the first column
// And then we'll merge all the conflicting couples to each columns

    function mergeConflictingCoupleToColumn(conflictedCouple, column) {
        var event1 = conflictedCouple[0];
        var event2 = conflictedCouple[1];

        var columnNumber = column > 0 ? column : 0;

        if(!eventsInColumns[columnNumber]) eventsInColumns[columnNumber] = [];

        solveConflict(event1, eventsInColumns[columnNumber]);

        solveConflict(event2, eventsInColumns[columnNumber]);

        function solveConflict(event, eventsColumn) {
            if (event.isInConflict && isEventsConflicting(event, eventsColumn).length <= 0) {
                delete event.isInConflict;
                eventsColumn.push(event);
                mergeConflictingCoupleToColumn(conflictedCouple, 0);
            }
        }

        if (event1.isInConflict || event2.isInConflict) {
            mergeConflictingCoupleToColumn(conflictedCouple, columnNumber + 1);
        }
    }

    function sortEvents() {
        extractConflictingEvents(events);

        extractNonConflictingEvents(events);

        eventsInColumns[0] = nonConflictingEvents.length > 0 ? nonConflictingEvents : [];

        for (var i = 0; i < conflictingCouples.length; i++) {
            mergeConflictingCoupleToColumn(conflictingCouples[i], 0);
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////// Find out the width of each event element

// We now know on which column each event should be positioned, but we don't know how much space it should take on the line
// At this stage, we know:
//  - how many columns we have
//  - which element should be on each column
// So we find out the width of each event by first setting the width of elements on the last columns => an element in the last column has a width of:
// containerWidth / number of columns
// Then we move from right to left and check for conflicts (all events conflicting with an event that already has a width gets the same width)

    function computeEventsWidth() {
        var arrayOfAllConflicts = [];
        for(var i = eventsInColumns.length - 1; i > 0; i--) {
            var width = 1 / (i + 1);
            eventsInColumns[i].forEach(function(event){
                !event.width ? event.width = width : '';
                // if 1 element in column n and 1 element in column n-1 are conflicting, it's the cumulated height of
                // the 2 elements that should be checked against column n-2
                var cumulatedHeightOfConflictingEvents = event;
                // We store all the event conflicting on a same "line", we need this later to set the smallest width to all events of the line
                var conflictsArray = [event];

                var previousColumn = i-1;
                if(previousColumn >= 0) reviewPreviousColumn(event, previousColumn);

                function reviewPreviousColumn(sourceEvent, previousColumnNumber){
                    var conflictingEventsInPreviousColumn = isEventsConflicting(sourceEvent, eventsInColumns[previousColumnNumber]);
                    if(conflictingEventsInPreviousColumn.length > 0) {
                        conflictsArray = conflictsArray.concat(conflictingEventsInPreviousColumn);
                        conflictingEventsInPreviousColumn.forEach(function(conflictingEvent) {
                            cumulatedHeightOfConflictingEvents = {
                                start: Math.min(conflictingEvent.start, sourceEvent.start),
                                end: Math.max(conflictingEvent.end, sourceEvent.end)
                            };
                            if(previousColumnNumber - 1 >= 0) reviewPreviousColumn(cumulatedHeightOfConflictingEvents, previousColumnNumber - 1);
                        })
                    } else {
                        if(previousColumnNumber - 1 >= 0) reviewPreviousColumn(cumulatedHeightOfConflictingEvents, previousColumnNumber - 1);
                    }

                    // Sort out the conflicts to find the event with the smallest width, and apply it to the whole conflict chain

                    setSmallestWidth(conflictsArray);

                    arrayOfAllConflicts.push(conflictsArray);
                }
            });
        }

        console.log('arrayOfAllConflicts', arrayOfAllConflicts);

        arrayOfAllConflicts.forEach(function(conflictsArray){
            setSmallestWidth(conflictsArray);
        });
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////// Create the event component, style it and add it to the DOM

    var containerWidth = 620;
    var containerHeight = 720;
    var containerPadding = 10;

    var elementPadding = 10;
    var elementBorder = 2;

    var minutesInADay = 12 * 60;

    function convertEventIntoPixel(event) {
        return {
            start: event.start * containerHeight / minutesInADay,
            end: event.end * containerHeight / minutesInADay
        }
    }

    function styleEventElement(event, eventElement, columnNumber) {
        eventElement.setAttribute('class', 'event');
        eventElement.innerHTML = event.start + " - " + event.end;
        eventElement.style.width = containerWidth * event.width - elementPadding - elementBorder + "px";
        eventElement.style.height = event.end - event.start + 'px';
        eventElement.style.top = convertEventIntoPixel(event).start + "px";
        eventElement.style.left = containerPadding + columnNumber * event.width * containerWidth + "px";

        return eventElement;
    }

    function createEventElement(event, container, columnNumber) {
        var eventElement = document.createElement('div');

        styleEventElement(event, eventElement, columnNumber);

        container.appendChild(eventElement);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    sortEvents();

    var container = document.getElementById("container");

    console.log('eventsInColumns', eventsInColumns);

    computeEventsWidth();

    eventsInColumns.forEach(function(columnOfEvents, columnNumber){
        columnOfEvents.forEach(function(event){
            createEventElement(event, container, columnNumber);
        })
    });
};

window.onload = layOutDay;